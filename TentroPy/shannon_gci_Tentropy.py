#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Shannon GCI based transfer entropy
'''


import sys
import numpy as np
from TentroPy.tent_lib import delayVectors, distanceMat
from joblib import Parallel, delayed
import multiprocessing
from tqdm import tqdm
import itertools


def shannon_gci_tentropy(source, target, h, ms=[1], mt=[1], tau=1, u=1,
                         norm='euclidean2', Tn=1, bias=True, cores_=8,
                         pbar=False):
    h = h**2
    m = list(itertools.product(ms, mt))
    M = len(m)
    H = len(h)
    T = np.zeros((H, M))
    ind = 0

    # Data normalization
    source = (source - np.mean(source)) / np.std(source)
    target = (target - np.mean(target)) / np.std(target)

    with Parallel(n_jobs=cores_) as parallel:
        for i in tqdm(iterable=m, total=M, file=sys.stdout, disable=not pbar):
            T[:, ind] = tranferEntropy(source, target, h, i[0], i[1], tau, u,
                                       norm, Tn, bias, pbar, parallel)
            ind = ind + 1
    return T, m


def tranferEntropy(source, target, h, ms, mt, tau, u, norm, Tn, bias, pbar,
                   parallel):
    # Get delay vectors
    ind0 = (mt - ms) * tau - u + 1
    if ind0 >= 0:
        Vt = delayVectors(target, mt, tau)
        T = delayVectors(target[(mt - 1) * tau + 1:], 1, 1)
        Vs = delayVectors(source[ind0:], ms, tau)
    else:
        ind0 = abs(ind0)
        Vt = delayVectors(target[ind0:], mt, tau)
        T = delayVectors(target[(mt - 1) * tau + 1 + ind0:], 1, 1)
        Vs = delayVectors(source, ms, tau)

    lT, _ = T.shape
    ls, _ = Vs.shape
    lt, _ = Vt.shape
    Vs = Vs[:-(ls - lT), :]
    Vt = Vt[:-(lt - lT), :]

    # Calc distances
    Z0 = (-1) * distanceMat(np.concatenate((Vs, Vt, T), axis=1), norm, Tn=Tn,
                            bias=bias)
    Z1 = (-1) * distanceMat(np.concatenate((Vs, Vt), axis=1), norm, Tn=Tn,
                            bias=bias)
    Z2 = (-1) * distanceMat(np.concatenate((Vt, T), axis=1), norm, Tn=Tn,
                            bias=bias)
    Z3 = (-1) * distanceMat(Vt, norm, Tn=Tn, bias=bias)

    # Calc gci
    lgci = parallel(delayed(Gci_eval)(z0=Z0, z1=Z1, z2=Z2, z3=Z3, h=j)
                    for j in h)
    lgci = np.asarray(lgci)
    Tent = (lgci[:, 0] - lgci[:, 1] - lgci[:, 2] + lgci[:, 3]) / tau

    return Tent


def Gci_eval(z0, z1, z2, z3, h):
    with np.errstate(divide='ignore', invalid='ignore'):
        g0 = np.mean(np.log(np.mean(np.exp(z0 / h), axis=1)))
        g1 = np.mean(np.log(np.mean(np.exp(z1 / h), axis=1)))
        g2 = np.mean(np.log(np.mean(np.exp(z2 / h), axis=1)))
        g3 = np.mean(np.log(np.mean(np.exp(z3 / h), axis=1)))
    return g0, g1, g2, g3
