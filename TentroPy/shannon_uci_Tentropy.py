#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Shannon UCI based transfer entropy
'''


import sys
import numpy as np
from joblib import Parallel, delayed
from TentroPy.tent_lib import delayVectors, distanceMat
import multiprocessing
from tqdm import tqdm
import itertools
import mpmath as mp
import scipy.interpolate as ip              # Data interpolator


mp.mp.dps = 25


def shannon_uci_tentropy(source, target, h, ms=[1], mt=[1], tau=1, u=1,
                         norm='euclidean2', Tn=1, bias=True, cores_=8,
                         pbar=False):
    h = h**2
    m = list(itertools.product(ms, mt))
    M = len(m)
    H = len(h)
    T = np.zeros((H, M))
    ind = 0

    # Data normalization
    source = (source - np.mean(source)) / np.std(source)
    target = (target - np.mean(target)) / np.std(target)

    with Parallel(n_jobs=cores_) as parallel:
        for i in tqdm(iterable=m, total=M, file=sys.stdout, disable=not pbar):
            T[:, ind] = tranferEntropy(source, target, h, i[0], i[1], tau, u,
                                       norm, Tn, bias, pbar, parallel)
            ind = ind + 1
    return T, m


def tranferEntropy(source, target, h, ms, mt, tau, u, norm, Tn, bias, pbar,
                   parallel):
    # Get delay vectors
    ind0 = (mt - ms) * tau - u + 1
    if ind0 >= 0:
        Vt = delayVectors(target, mt, tau)
        T = delayVectors(target[(mt - 1) * tau + 1:], 1, 1)
        Vs = delayVectors(source[ind0:], ms, tau)
    else:
        ind0 = abs(ind0)
        Vt = delayVectors(target[ind0:], mt, tau)
        T = delayVectors(target[(mt - 1) * tau + 1 + ind0:], 1, 1)
        Vs = delayVectors(source, ms, tau)

    lT, _ = T.shape
    ls, _ = Vs.shape
    lt, _ = Vt.shape
    Vs = Vs[:-(ls - lT), :]
    Vt = Vt[:-(lt - lT), :]

    # Calc distances
    Z0 = distanceMat(np.concatenate((Vs, Vt, T), axis=1), norm, Tn=Tn,
                     bias=bias)
    Z1 = distanceMat(np.concatenate((Vs, Vt), axis=1), norm, Tn=Tn,
                     bias=bias)
    Z2 = distanceMat(np.concatenate((Vt, T), axis=1), norm, Tn=Tn,
                     bias=bias)
    Z3 = distanceMat(Vt, norm, Tn=Tn, bias=bias)

    # Calc uci
    t = 10**np.concatenate(([-10], np.linspace(-5, 5, 300), [10]))

    f0 = [mp.gammainc((ms + mt + 1) / 2, a=x, regularized=True) for x in t]
    f1 = [mp.gammainc((ms + mt) / 2, a=x, regularized=True) for x in t]
    f2 = [mp.gammainc((mt + 1) / 2, a=x, regularized=True) for x in t]
    f3 = [mp.gammainc(mt / 2, a=x, regularized=True) for x in t]
    K0 = ip.InterpolatedUnivariateSpline(t, f0)
    K1 = ip.InterpolatedUnivariateSpline(t, f1)
    K2 = ip.InterpolatedUnivariateSpline(t, f2)
    K3 = ip.InterpolatedUnivariateSpline(t, f3)

    luci = parallel(delayed(Uci_eval)(z0=Z0, z1=Z1, z2=Z2, z3=Z3, k0=K0, k1=K1,
                                      k2=K2, k3=K3, h=j) for j in h)
    luci = np.asarray(luci)
    Tent = (luci[:, 0] - luci[:, 1] - luci[:, 2] + luci[:, 3]) / tau
    return Tent


def Uci_eval(z0, z1, z2, z3, k0, k1, k2, k3, h):
    with np.errstate(divide='ignore', invalid='ignore'):
        u0 = np.mean(np.log(np.mean(k0(z0 / h), axis=1)))
        u1 = np.mean(np.log(np.mean(k1(z1 / h), axis=1)))
        u2 = np.mean(np.log(np.mean(k2(z2 / h), axis=1)))
        u3 = np.mean(np.log(np.mean(k3(z3 / h), axis=1)))
    return u0, u1, u2, u3
