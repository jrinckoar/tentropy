#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from gci_Tentropy import gci_tentropy
from uci_Tentropy import uci_tentropy
from shannon_gci_Tentropy import shannon_gci_tentropy
from shannon_uci_Tentropy import shannon_uci_tentropy


np.errstate(divide='ignore', invalid='ignore')

def plot_fun(Tyx, Txy, h, myx, mxy, title):
    plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((1, 3), (0, 0))
    LineObj = ax1.plot(np.log(h), Tyx, lw=2, label=' ')
    ax1.set_ylabel('Tentropy Y -> X')
    ax1.set_xlabel('ln(h)')
    ax1.legend(iter(LineObj), (myx), loc=0)
    plt.legend()

    ax2 = plt.subplot2grid((1, 3), (0, 1), sharex=ax1, sharey=ax1)
    LineObj = ax2.plot(np.log(h), Txy, lw=2, label=' ')
    ax2.set_ylabel('Tentropy X -> Y')
    ax2.set_xlabel('ln(h)')
    ax2.legend(iter(LineObj), (mxy), loc=0)
    plt.legend()

    ax3 = plt.subplot2grid((1, 3), (0, 2), sharex=ax1)
    LineObj = ax3.plot(np.log(h), Txy - Tyx, lw=2)
    ax3.set_ylabel('Tx -> y - Ty -> x')
    ax3.set_xlabel('ln(h)')
    plt.suptitle(title)


def AR_model():
    # T-entropy parameters
    print('AR model')
    N = 2000
    ms = [1, 2, 4, 10]
    mt = [2]
    tau = 1
    h = np.exp(np.linspace(-4, 2, 250))
    u = 1

    # AR model
    p = 2
    alpha = [-0.5, 0.5]
    beta = [-0.5, 0.5]
    nl = 0.1                        # Noise level
    x = np.random.normal(size=N)
    y = np.zeros(N)
    y[0:p] = x[0:p]
    for i in range(p, N):
        y[i] = np.sum(alpha * y[i - p:i]) + np.sum(beta * x[i - p: i]) \
            + nl * np.random.normal(1)

    UTyx, Umyx = uci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau)
    UTxy, Umxy = uci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau)

    GTyx, Gmyx = gci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau)
    GTxy, Gmxy = gci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau)

    plot_fun(UTyx, UTxy, h, Umxy, Umyx, 'AR Model T-Entropy UCI')
    plot_fun(GTyx, GTxy, h, Gmxy, Gmyx, 'AR-Model T-Entropy GCI')


def AR_model2():
    print('AR^2 model')
    # T-entropy parameters
    N = 2000
    ms = [1, 2, 4, 10]
    mt = [2]
    tau = 1
    h = np.exp(np.linspace(-4, 2, 250))
    u = 1

    # AR^2 model
    p = 2
    alpha = [-0.5, 0.5]
    beta = [-0.5, 0.5]
    nl = 0.1                        # Noise level
    x = np.random.normal(size=N)
    y = np.zeros(N)
    y[0:p] = x[0:p]
    for i in range(p, N):
        y[i] = np.sum(alpha * y[i - p:i]) + np.sum(beta * x[i - p: i]**2) + nl * np.random.normal(1)

    UTyx, Umyx = uci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau)
    UTxy, Umxy = uci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau)

    GTyx, Gmyx = gci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau)
    GTxy, Gmxy = gci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau)

    plot_fun(UTyx, UTxy, h, Umxy, Umyx, 'AR^2 Model T-Entropy UCI')
    plot_fun(GTyx, GTxy, h, Gmxy, Gmyx, 'AR^2 Model T-Entropy GCI')


def Coupled_Henon(eps=0):
    print('Coupled Henon model eps={0}'.format(eps))
    # T-entropy parameters
    N = 2000
    ms = [1, 2, 4, 10]
    mt = [1, 2]
    tau = 1
    h = np.exp(np.linspace(-8, 2, 100))
    u = 1
    norm = 'euclidean2'
    Tn = 4
    bias = True

    # Coupled Henon model
    x = np.zeros(N + 10000)
    y = np.zeros(N + 10000)
    x[:2] = np.random.uniform(0, 1, 2)
    flag = True
    while (flag):
        for i in range(2, N + 10000):
            x[i] = 1.4 - x[i - 1]**2 + 0.3 * x[i - 2]
            y[i] = 1.4 - (eps * x[i - 1] + (1 - eps) * y[i - 1]) * y[i - 1] \
                + 0.3 * y[i - 2]
        if (np.isinf(y).any() and np.isinf(x).any()):
            x[:2] = np.random.uniform(0, 1, 2)
            y[:2] = np.random.uniform(0, 1, 2)
        else:
            flag = False

    x = x[10000:]
    y = y[10000:]

    # print('GCI')
    # GTyx, Gmyx, GSyncyx = gci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau)
    # GTxy, Gmxy, GSyncxy = gci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau)

    # plot_fun(GTyx, GTxy, h, Gmyx, Gmxy,
             # 'Coupled Henon Model T-Entropy Renyi GCI eps={0}'.format(eps))

    # # print('UCI')
    # # UTyx, Umyx, USyncyx = uci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau)
    # # UTxy, Umxy, USyncxy = uci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau)

    # print('Shannon GCI')
    # SgTyx, Sgmyx = shannon_gci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau,
                                        # norm=norm, Tn=Tn, bias=bias)
    # SgTxy, Sgmxy = shannon_gci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau,
                                        # norm=norm, Tn=Tn, bias=bias)

    # plot_fun(SgTyx, SgTxy, h, Sgmyx, Sgmxy,
             # 'Coupled Henon Model T-Entropy Shannon GCI eps={0}'.format(eps))

    print('Shannon UCI')
    SuTyx, Sumyx = shannon_uci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau,
                                        norm=norm, Tn=Tn, bias=bias)
    SuTxy, Sumxy = shannon_uci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau,
                                        norm=norm, Tn=Tn, bias=bias)

    plot_fun(SuTyx, SuTxy, h, Sumyx, Sumxy,
             'Coupled Henon Model T-Entropy Shannon UCI eps={0}'.format(eps))


def Coupled_Rossler(w1, w2, eps, dt=0.05):
    print('Coupled Rossler model eps={0}'.format(eps))
    N = 2000
    ms = [1, 2, 3, 4]
    mt = [3]
    tau = 4
    h = np.exp(np.linspace(-8, 2, 100))
    u = 1
    norm = 'euclidean2'
    Tn = 4
    bias = False
    bias = True

    Nt = N + 10000
    tf = dt * Nt
    t = np.arange(0, tf, dt)
    x0 = np.random.random(6)
    sol = odeint(funRoss, x0, t, args=(w1, w2, eps))
    x, _, _, y, _, _ = zip(*sol)
    x = np.array(x[10000:])
    y = np.array(y[10000:])

    print('GCI')
    GTyx, Gmyx, GSyncyx = gci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau)
    GTxy, Gmxy, GSyncxy = gci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau)

    # print('UCI')
    # UTyx, Umyx, USyncyx = uci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau)
    # UTxy, Umxy, USyncxy = uci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau)

    print('Shannon GCI')
    STyx, Smyx = shannon_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau,
                                  norm=norm, Tn=Tn, bias=bias)
    STxy, Smxy = shannon_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau,
                                  norm=norm, Tn=Tn, bias=bias)

    plot_fun(STyx, STxy, h, Smyx, Smxy,
             'Coupled Henon Model T-Entropy Shannon eps={0}'.format(eps))

    plot_fun(GTyx, GTxy, h, Gmyx, Gmxy,
             'Coupled Henon Model T-Entropy Renyi GCI eps={0}'.format(eps))


def funRoss(state, t, w1, w2, eps):
    x1, x2, x3, y1, y2, y3 = state
    dx1 = -w1 * x2 - x3
    dx2 = w1 * x1 + 0.15 * x2
    dx3 = 0.2 + x3 * (x1 - 10)
    dy1 = -w2 * y2 - y3 + eps * (x1 - y1)
    dy2 = w2 * y1 + 0.15 * y2
    dy3 = 0.2 + y3 * (y1 - 10)
    return dx1, dx2, dx3, dy1, dy2, dy3


if __name__ == '__main__':

    # AR_model()
    # AR_model2()
    Coupled_Henon(eps=0.0)
    Coupled_Henon(eps=0.5)
    Coupled_Henon(eps=0.8)
    # Coupled_Rossler(w1=1.015, w2=0.985, eps=0.00)
    # Coupled_Rossler(w1=1.015, w2=0.985, eps=0.24)
    # Coupled_Rossler(w1=0.5, w2=2.515, eps=0.08)
    plt.show()
