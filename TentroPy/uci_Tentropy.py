#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
UCI based transfer entropy
'''

import sys
import numpy as np
import matplotlib.pyplot as plt
from TentroPy.tent_lib import delayVectors, distanceVec, surrogate, wavediff
from joblib import Parallel, delayed
import multiprocessing
from tqdm import tqdm
import itertools
import mpmath as mp
import scipy.interpolate as ip              # Data interpolator


mp.mp.dps = 25


def uci_tentropy(source, target, h, ms=[1], mt=[1], tau=1, u=1,
                 norm='euclidean2', Tn=1, cores_=8, pbar=True):
    h = h**2
    m = list(itertools.product(ms, mt))
    M = len(m)
    H = len(h)
    T = np.zeros((H, M))
    T_s = np.zeros((H, M))
    Sync = np.zeros((H, M))
    ind = 0

    # Data normalization
    source = (source - np.mean(source)) / np.std(source)
    target = (target - np.mean(target)) / np.std(target)

    # Surrogate data
    source_s = surrogate(source)

    with Parallel(n_jobs=cores_) as parallel:
        for i in tqdm(iterable=m, total=M, file=sys.stdout, disable=not pbar):
            T[:, ind], Sync[:, ind] = tranferEntropy(source, target, h, i[0],
                                                     i[1], tau, u, norm, Tn,
                                                     pbar, parallel)
            T_s[:, ind], _ = tranferEntropy(source_s, target, h, i[0], i[1],
                                            tau, u, norm, Tn, pbar, parallel)
            ind = ind + 1
    return T - T_s, m, Sync


def tranferEntropy(source, target, h, ms, mt, tau, u, norm, Tn, pbar,
                   parallel):
    # Get delay vectors
    ind0 = (mt - ms) * tau - u + 1
    if ind0 >= 0:
        Vt = delayVectors(target, mt, tau)
        T = delayVectors(target[(mt - 1) * tau + 1:], 1, 1)
        Vs = delayVectors(source[ind0:], ms, tau)
    else:
        ind0 = abs(ind0)
        Vt = delayVectors(target[ind0:], mt, tau)
        T = delayVectors(target[(mt - 1) * tau + 1 + ind0:], 1, 1)
        Vs = delayVectors(source, ms, tau)

    lT, _ = T.shape
    ls, _ = Vs.shape
    lt, _ = Vt.shape
    Vs = Vs[:-(ls - lT), :]
    Vt = Vt[:-(lt - lT), :]

    # Calc distances
    Z0 = distanceVec(np.concatenate((Vs, Vt, T), axis=1), norm, Tn=Tn)
    Z1 = distanceVec(np.concatenate((Vs, Vt), axis=1), norm, Tn=Tn)
    Z2 = distanceVec(np.concatenate((Vt, T), axis=1), norm, Tn=Tn)
    Z3 = distanceVec(Vt, norm, Tn=Tn)
    Z4 = distanceVec(Vs, norm, Tn=Tn)

    # Calc uci
    t = 10**np.concatenate(([-10], np.linspace(-5, 5, 300), [10]))

    f0 = [mp.gammainc((ms + mt + 1) / 2, a=x, regularized=True) for x in t]
    f1 = [mp.gammainc((ms + mt) / 2, a=x, regularized=True) for x in t]
    f2 = [mp.gammainc((mt + 1) / 2, a=x, regularized=True) for x in t]
    f3 = [mp.gammainc(mt / 2, a=x, regularized=True) for x in t]
    f4 = [mp.gammainc(ms / 2, a=x, regularized=True) for x in t]
    K0 = ip.InterpolatedUnivariateSpline(t, f0)
    K1 = ip.InterpolatedUnivariateSpline(t, f1)
    K2 = ip.InterpolatedUnivariateSpline(t, f2)
    K3 = ip.InterpolatedUnivariateSpline(t, f3)
    K4 = ip.InterpolatedUnivariateSpline(t, f4)

    uci = parallel(delayed(Uci_eval)(z0=Z0, z1=Z1, z2=Z2, z3=Z3, z4=Z4,
                                     k0=K0, k1=K1, k2=K2, k3=K3, k4=K4,
                                     h=j) for j in h)
    uci = np.asarray(uci)

    Tent = np.log((uci[:, 0] * uci[:, 3]) / (uci[:, 1] * uci[:, 2]))

    # Calc correlation dimension
    D1 = h * (wavediff(uci[:, 1]) / wavediff(h)) / uci[:, 1]
    D3 = h * (wavediff(uci[:, 3]) / wavediff(h)) / uci[:, 3]
    D4 = h * (wavediff(uci[:, 4]) / wavediff(h)) / uci[:, 4]

    # Sync = np.log((D3 + D4) / (D1))
    Sync = D1 / (D3 + D4)

    return Tent, Sync


def Uci_eval(z0, z1, z2, z3, z4, k0, k1, k2, k3, k4, h):
    u0 = np.mean(k0(z0 / h))
    u1 = np.mean(k1(z1 / h))
    u2 = np.mean(k2(z2 / h))
    u3 = np.mean(k3(z3 / h))
    u4 = np.mean(k4(z4 / h))
    return u0, u1, u2, u3, u4






# def uci_tentropy(source, target, h, ms=[1], mt=[1], tau=1, u=1,
                 # norm='euclidean2', Tn=0, cores_=8, pbar=False):
    # h = h**2
    # m = list(itertools.product(ms, mt))
    # M = len(m)
    # H = len(h)
    # T = np.zeros((H, M))
    # T_s = np.zeros((H, M))
    # Sync = np.zeros((H, M))
    # ind = 0
    # source_s = surrogate(source)

    # Data normalization
    # source = (source - np.mean(source)) / np.std(source)
    # source_s = (source_s - np.mean(source_s)) / np.std(source_s)
    # target = (target - np.mean(target)) / np.std(target)

    # with Parallel(n_jobs=cores_) as parallel:
        # for i in tqdm(iterable=m, total=M, file=sys.stdout, disable=pbar):
            # T[:, ind], Sync[:, ind] = tranferEntropy(source, target, h, i[0],
                                                     # i[1], tau, u, norm, Tn,
                                                     # pbar, parallel)
            # T_s[:, ind], _ = tranferEntropy(source_s, target, h, i[0], i[1],
                                            # tau, u, norm, Tn, pbar, parallel)
            # ind = ind + 1
    # return T - T_s, m, Sync


# def tranferEntropy(source, target, h, ms, mt, tau, u, norm, Tn, pbar,
                   # parallel):

    # Get delay vectors
    # ind0 = (mt - ms) * tau - u + 1
    # if ind0 >= 0:
        # Vt = delayVectors(target, mt, tau)
        # T = delayVectors(target[(mt - 1) * tau + 1:], 1, 1)
        # Vs = delayVectors(source[ind0:], ms, tau)
    # else:
        # ind0 = abs(ind0)
        # Vt = delayVectors(target[ind0:], mt, tau)
        # T = delayVectors(target[(mt - 1) * tau + 1 + ind0:], 1, 1)
        # Vs = delayVectors(source, ms, tau)

    # lT, _ = T.shape
    # ls, _ = Vs.shape
    # lt, _ = Vt.shape
    # Vs = Vs[:-(ls - lT), :]
    # Vt = Vt[:-(lt - lT), :]

    # Interpolation range
    # t = 10**np.concatenate(([-14], np.linspace(-8, 5, 300), [8]))
    # t = np.concatenate(([10**-10], np.logspace(-8, 5, 300), [10**10]))
    # t = np.exp(np.concatenate(([-15], np.linspace(-5, 10, 500), [15])))
    # t = 10**np.concatenate(([-10], np.linspace(-5, 5, 300), [10]))
    # Calc distances
    # Z0 = (-1) * distanceVec(np.concatenate((Vs, Vt, T), axis=1), norm, Tn=Tn)
    # Z1 = (-1) * distanceVec(np.concatenate((Vs, Vt), axis=1), norm, Tn=Tn)
    # Z2 = (-1) * distanceVec(np.concatenate((Vt, T), axis=1), norm, Tn=Tn)
    # Z3 = (-1) * distanceVec(Vt, norm, Tn=Tn)
    # Z4 = (-1) * distanceVec(Vs, norm, Tn=Tn)

    # beta = 2
    # f0 = gammaincc(beta / 2, t)
    # f1 = gammaincc(beta / 2, t)
    # f2 = gammaincc(beta / 2, t)
    # f3 = gammaincc(beta / 2, t)
    # f4 = gammaincc(beta / 2, t)
    # f0 = [mp.gammainc(beta / 2, a=x, regularized=True) for x in t]
    # f1 = [mp.gammainc(beta / 2, a=x, regularized=True) for x in t]
    # f2 = [mp.gammainc(beta / 2, a=x, regularized=True) for x in t]
    # f3 = [mp.gammainc(beta / 2, a=x, regularized=True) for x in t]
    # f4 = [mp.gammainc(beta / 2, a=x, regularized=True) for x in t]
    # f0 = [np.exp(-x) for x in t]
    # f1 = [np.exp(-x) for x in t]
    # f2 = [np.exp(-x) for x in t]
    # f3 = [np.exp(-x) for x in t]
    # f4 = [np.exp(-x) for x in t]
    # K0 = ip.InterpolatedUnivariateSpline(t, f0)
    # K1 = ip.InterpolatedUnivariateSpline(t, f1)
    # K2 = ip.InterpolatedUnivariateSpline(t, f2)
    # K3 = ip.InterpolatedUnivariateSpline(t, f3)
    # K4 = ip.InterpolatedUnivariateSpline(t, f4)

    # uci = parallel(delayed(Uci_eval)(z0=Z0, z1=Z1, z2=Z2, z3=Z3, z4=Z4,
                                     # k0=K0, k1=K1, k2=K2, k3=K3, k4=K4,
                                     # h=j) for j in h)
    # uci = np.asarray(uci)

    # Tent = np.log((uci[:, 0] * uci[:, 3]) / (uci[:, 1] * uci[:, 2]))

    # Calc correlation dimension
    # D1 = h * (wavediff(uci[:, 1]) / wavediff(h)) / uci[:, 1]
    # D3 = h * (wavediff(uci[:, 3]) / wavediff(h)) / uci[:, 3]
    # D4 = h * (wavediff(uci[:, 4]) / wavediff(h)) / uci[:, 4]

    # Sync = np.log((D3 + D4) / (D1))

    # return Tent, Sync


# def Uci_eval(z0, z1, z2, z3, z4, k0, k1, k2, k3, k4, h):
    # u0 = np.mean(np.exp(z0 / h))
    # u1 = np.mean(np.exp(z1 / h))
    # u2 = np.mean(np.exp(z2 / h))
    # u3 = np.mean(np.exp(z3 / h))
    # u4 = np.mean(np.exp(z4 / h))
    # return u0, u1, u2, u3, u4

# def Uci_eval(z0, z1, z2, z3, z4, k0, k1, k2, k3, k4, h):
    # # u0 = np.mean(k0(z0 / h))
    # # u1 = np.mean(k1(z1 / h))
    # # u2 = np.mean(k2(z2 / h))
    # # u3 = np.mean(k3(z3 / h))
    # # u4 = np.mean(k4(z4 / h))
    # u0 = np.mean(np.exp(z0 / h))
    # u1 = np.mean(np.exp(z1 / h))
    # u2 = np.mean(np.exp(z2 / h))
    # u3 = np.mean(np.exp(z3 / h))
    # u4 = np.mean(np.exp(z4 / h))
    # return u0, u1, u2, u3, u4

    # k0 = ip.InterpolatedUnivariateSpline(t, gammaincc((ms + mt + 1) / 2, t))
    # beta = (ms + mt + 1) / 2
    # beta = mp.mpf(2)
    # f = [mp.gammainc(beta, a=x, regularized=True) for x in t]
    # k0 = ip.InterpolatedUnivariateSpline(t, f)
    # uci0 = parallel(delayed(Uci_eval)(z=Z0, kernel=k0, h=j) for j in h)

    # k0 = ip.InterpolatedUnivariateSpline(t, gammaincc((ms + mt) / 2, t))
    # beta = (ms + mt) / 2
    # beta = mp.mpf(2)
    # f = [mp.gammainc(beta, a=x, regularized=True) for x in t]
    # k0 = ip.InterpolatedUnivariateSpline(t, f)
    # uci1 = parallel(delayed(Uci_eval)(z=Z1, kernel=k0, h=j) for j in h)

    # # Z = distanceVec(np.concatenate((Vt, T), axis=1), norm, Tn)
    # # k0 = ip.InterpolatedUnivariateSpline(t, gammaincc((mt + 1) / 2, t))
    # # beta = (mt + 1) / 2
    # beta = mp.mpf(2)
    # f = [mp.gammainc(beta, a=x, regularized=True) for x in t]
    # k0 = ip.InterpolatedUnivariateSpline(t, f)
    # uci2 = parallel(delayed(Uci_eval)(z=Z2, kernel=k0, h=j) for j in h)

    # # Z = distanceVec(Vt, norm, Tn)
    # # k0 = ip.InterpolatedUnivariateSpline(t, gammaincc(mt / 2, t))
    # # beta = mt / 2
    # beta = mp.mpf(2)
    # f = [mp.gammainc(beta, a=x, regularized=True) for x in t]
    # k0 = ip.InterpolatedUnivariateSpline(t, f)
    # uci3 = parallel(delayed(Uci_eval)(z=Z3, kernel=k0, h=j) for j in h)

    # # Z = distanceVec(Vs, norm, Tn)
    # # k0 = ip.InterpolatedUnivariateSpline(t, gammaincc(ms / 2, t))
    # beta = mp.mpf(2)
    # # beta = ms / 2
    # f = [mp.gammainc(beta, a=x, regularized=True) for x in t]
    # k0 = ip.InterpolatedUnivariateSpline(t, f)
    # uci4 = parallel(delayed(Uci_eval)(z=Z4, kernel=k0, h=j) for j in h)

    # uci0 = np.asarray(uci0)
    # uci1 = np.asarray(uci1)
    # uci2 = np.asarray(uci2)
    # uci3 = np.asarray(uci3)
    # uci4 = np.asarray(uci4)

    # Tent = np.log(uci0 * uci3 / (uci1 * uci2))

    # # Calc correlation dimension
    # D1 = h * (wavediff(uci1) / wavediff(h)) / uci1
    # D3 = h * (wavediff(uci3) / wavediff(h)) / uci3
    # D4 = h * (wavediff(uci4) / wavediff(h)) / uci4

    # Sync = np.log((D3 + D4) / (D1))

    # return Tent, Sync




# if __name__ == '__main__':

    # N = 2000
    # u = 1
    # mt = [2]
    # ms = [1, 2, 6, 8]
    # tau = 1
    # h = np.exp(np.linspace(-2.5, 1, 150))

    # x = np.linspace(0, 9, 10)
    # y = np.linspace(10, 19, 10)
    # Tyx, Txy, = uci_tentropy(x, y, h, mx=m, my=m, u=1)

    # AR model
    # p = 2
    # alpha = [-0.5, 0.5]
    # beta = [-0.5, 0.5]
    # nl = 0.00                        Noise level

    # x = np.random.normal(size=N)
    # y = np.zeros(N)
    # y[0:p] = np.random.normal(size=p)
    # for i in range(p, N):
        # y[i] = np.sum(alpha * y[i - p:i]) + np.sum(beta * x[i - p: i])
        # + nl * np.random.normal(1)

    # Tyx, myx = uci_tentropy(y, x, h, ms=ms, mt=mt, u=u, tau=tau)
    # Txy, mxy = uci_tentropy(x, y, h, ms=ms, mt=mt, u=u, tau=tau)

    # plt.figure(figsize=plt.figaspect(0.5))
    # ax1 = plt.subplot2grid((1, 1), (0, 0))
    # LineObj = ax1.plot(np.log(h), Tyx, lw=2)
    # ax1.set_ylabel('Tentropy Y -> X')
    # ax1.set_xlabel('ln(h)')
    # ax1.legend(iter(LineObj), (myx), loc=0)
    # plt.legend()
    # plt.suptitle('UCI Transfer entropy')

    # plt.figure(figsize=plt.figaspect(0.5))
    # ax2 = plt.subplot2grid((1, 1), (0, 0), sharex=ax1, sharey=ax1)
    # LineObj = ax2.plot(np.log(h), Txy, lw=2)
    # ax2.set_ylabel('Tentropy X -> Y')
    # ax2.set_xlabel('ln(h)')
    # ax2.legend(iter(LineObj), (mxy), loc=0)
    # plt.legend()
    # plt.suptitle('UCI Transfer entropy')
    # plt.show()
    # pass
