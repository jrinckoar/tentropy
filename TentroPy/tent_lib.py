#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import numpy as np
import pywt
from matplotlib import pyplot as plt


def delayVectors(x, m, tau):
    m = int(m)
    L = len(x) - (m - 1) * tau
    sVec = np.zeros((L, m))
    k = (m - 1) * tau
    for i in range(L):
        sVec[i, :] = x[i: i + k + 1: tau]
    return sVec


def distanceVec(sVec, norm='euclidean2', Tn=0):

    V = np.array(sVec)
    Q = V.shape[0]
    L = int((Q - Tn) * (Q - Tn - 1) / 2)
    Z = np.zeros(L)
    c = 0
    for k in range(1, Q - Tn):
        v1 = V[0:Q - Tn - k, :]
        v2 = V[k + Tn:Q, :]
        if norm == 'euclidean':
            a = np.sum(v1**2, axis=1)
            b = np.sum(v2**2, axis=1)
            ab = np.sum(v1 * v2, axis=1)
            z = np.sqrt(a + b - 2 * ab)
        elif norm == 'euclidean2':
            a = np.sum(v1**2, axis=1)
            b = np.sum(v2**2, axis=1)
            ab = np.sum(v1 * v2, axis=1)
            z = a + b - 2 * ab
        elif norm == 'max':
            z = np.max(np.abs(v1 - v2), axis=1)
        i = len(z)
        Z[c:c + i] = z
        c = c + i
    return Z


def distanceMat(sVec, norm='euclidean2', Tn=0, bias=True):

    V = np.array(sVec)
    Q = V.shape[0]
    L = int(Q - Tn)
    Z = np.zeros((L, L))
    if not bias:
        row = np.array(range(0, L))
        Z[row, row] = 10000 * np.ones((L,))

    for k in range(1, Q - Tn):
        v1 = V[0:Q - Tn - k, :]
        v2 = V[k + Tn:Q, :]
        if norm == 'euclidean':
            a = np.sum(v1**2, axis=1)
            b = np.sum(v2**2, axis=1)
            ab = np.sum(v1 * v2, axis=1)
            z = np.sqrt(a + b - 2 * ab)
        elif norm == 'euclidean2':
            a = np.sum(v1**2, axis=1)
            b = np.sum(v2**2, axis=1)
            ab = np.sum(v1 * v2, axis=1)
            z = a + b - 2 * ab
        elif norm == 'max':
            z = np.max(np.abs(v1 - v2), axis=1)
        row = np.array(range(0, L - k))
        col = row + k
        Z[row, col] = z
    return Z + Z.T


def test_stateVectors():
    x = np.linspace(1, 10, 10)
    m_ = 2
    tau_ = 3
    dVec = delayVectors(x, m=m_, tau=tau_)
    V = distanceVec(dVec)
    M = distanceMat(dVec, bias=False)
    print('Series:\n {0}'.format(x))
    print('Delay Vectors m={0}, tau={1}:\n {2}'.format(m_, tau_, dVec))
    print('Distance Vector:\n {0}'.format(V))
    print('Distance Matrix:\n {0}'.format(M))


def surrogate(x):
    Fx = np.fft.rfft(x)
    t = np.random.normal(0, 1, len(x))
    Ft = np.fft.rfft(t)
    return np.fft.irfft(np.sqrt((Fx.real**2 + Fx.imag**2)) *
                        np.exp(Ft.imag * 1.0j))


def test_surrogate():
    N = 10000
    p = 2
    alpha = [-0.2, 0.2]
    beta = [-0.2, 0.2]
    nl = 0.00                        # Noise level

    x = np.random.normal(size=N)
    y = np.zeros(N)
    y[0:p] = np.random.normal(size=p)

    for i in range(p, N):
        y[i] = np.sum(alpha * y[i - p:i]) + np.sum(beta * x[i - p: i])
        + nl * np.random.normal(1)

    Fx = np.fft.fftshift(np.fft.fft(x))
    Fy = np.fft.fftshift(np.fft.fft(y))

    x_s = surrogate(x)
    y_s = surrogate(y)

    Fx_s = np.fft.fftshift(np.fft.fft(x_s))
    Fy_s = np.fft.fftshift(np.fft.fft(y_s))

    fx = np.linspace(-1, 1, len(Fx))
    fy = np.linspace(-1, 1, len(Fy))

    plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((1, 2), (0, 1))
    ax2 = plt.subplot2grid((1, 2), (0, 0))

    ax1.plot(fx, (Fx.real**2 + Fx.imag**2) / N, 'b', lw=2, label='Fx')
    ax1.plot(fx, (Fx_s.real**2 + Fx_s.imag**2) / N, '.r', lw=2, label='Fx_s')
    ax1.set_ylabel('|Ftt|^2')
    ax1.set_xlabel('f')

    ax2.plot(x, 'b', lw=2, label='Fx')
    ax2.plot(x_s, 'r', lw=1, label='Fy')
    ax2.set_ylabel('f(n)')
    ax2.set_xlabel('n')

    plt.suptitle('Surrogate Withe Noise')
    plt.legend()

    plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((1, 2), (0, 1))
    ax2 = plt.subplot2grid((1, 2), (0, 0))

    ax1.plot(fy, (Fy.real**2 + Fy.imag**2) / N, 'b', lw=2, label='Fy')
    ax1.plot(fy, (Fy_s.real**2 + Fy_s.imag**2) / N, '.r', lw=2, label='Fy_s')
    ax1.set_ylabel('|Ftt|^2')
    ax1.set_xlabel('f')

    ax2.plot(y, 'b', lw=2, label='Fx')
    ax2.plot(y_s, 'r', lw=1, label='Fy')
    ax2.set_ylabel('f(n)')
    ax2.set_xlabel('n')

    plt.suptitle('Surrogate AR')
    plt.legend()
    plt.show()


def wavediff(y, scale=2):
    dydt = np.zeros(y.shape)
    K = (2 * np.pi)**(1 / 4)
    yw, _ = pywt.cwt(y, scale, 'gaus1')
    dydt = yw / (scale**(3 / 2) * K)
    return np.squeeze(-dydt)


def test_wavediff():
    N = 1000
    sc = 8
    t = np.linspace(0, 16 * np.pi, N)
    y = np.sin(2 * t) + 0.05 * np.random.normal(0, 1, N)
    dy = wavediff(y, scale=sc)
    dt = wavediff(t, scale=sc)

    plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((1, 2), (0, 0))
    ax2 = plt.subplot2grid((1, 2), (0, 1))

    ax1.plot(t, y, 'b', label='y')
    ax2.plot(t, dy / dt, 'r', label='dy')
    plt.suptitle('Wavelet derivative')
    plt.show()


if __name__ == '__main__':
    # test_wavediff()
    test_stateVectors()
