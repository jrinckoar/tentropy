#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Renyi q=2 GCI based transfer entropy
'''

import sys
import numpy as np
from TentroPy.tent_lib import delayVectors, distanceVec, surrogate, wavediff
from joblib import Parallel, delayed
import multiprocessing
from tqdm import tqdm
import itertools


def gci_tentropy(source, target, h, ms=[1], mt=[1], tau=1, u=1,
                 norm='euclidean2', Tn=1, cores_=8, pbar=True):
    h = h**2
    m = list(itertools.product(ms, mt))
    M = len(m)
    H = len(h)
    T = np.zeros((H, M))
    T_s = np.zeros((H, M))
    Sync = np.zeros((H, M))
    ind = 0

    # Data normalization
    source = (source - np.mean(source)) / np.std(source)
    target = (target - np.mean(target)) / np.std(target)

    # Surrogate data
    source_s = surrogate(source)

    with Parallel(n_jobs=cores_) as parallel:
        for i in tqdm(iterable=m, total=M, file=sys.stdout, disable=not pbar):
            T[:, ind], Sync[:, ind] = tranferEntropy(source, target, h, i[0],
                                                     i[1], tau, u, norm, Tn,
                                                     pbar, parallel)
            T_s[:, ind], _ = tranferEntropy(source_s, target, h, i[0], i[1],
                                            tau, u, norm, Tn, pbar, parallel)
            ind = ind + 1
    return T - T_s, m, Sync


def tranferEntropy(source, target, h, ms, mt, tau, u, norm, Tn, pbar,
                   parallel):
    # Get delay vectors
    ind0 = (mt - ms) * tau - u + 1
    if ind0 >= 0:
        Vt = delayVectors(target, mt, tau)
        T = delayVectors(target[(mt - 1) * tau + 1:], 1, 1)
        Vs = delayVectors(source[ind0:], ms, tau)
    else:
        ind0 = abs(ind0)
        Vt = delayVectors(target[ind0:], mt, tau)
        T = delayVectors(target[(mt - 1) * tau + 1 + ind0:], 1, 1)
        Vs = delayVectors(source, ms, tau)

    lT, _ = T.shape
    ls, _ = Vs.shape
    lt, _ = Vt.shape
    Vs = Vs[:-(ls - lT), :]
    Vt = Vt[:-(lt - lT), :]

    # Calc distances
    Z0 = (-1) * distanceVec(np.concatenate((Vs, Vt, T), axis=1), norm, Tn=Tn)
    Z1 = (-1) * distanceVec(np.concatenate((Vs, Vt), axis=1), norm, Tn=Tn)
    Z2 = (-1) * distanceVec(np.concatenate((Vt, T), axis=1), norm, Tn=Tn)
    Z3 = (-1) * distanceVec(Vt, norm, Tn=Tn)
    Z4 = (-1) * distanceVec(Vs, norm, Tn=Tn)
    # Calc gci
    gci = parallel(delayed(Gci_eval)(z0=Z0, z1=Z1, z2=Z2, z3=Z3, z4=Z4,
                                     h=j) for j in h)
    gci = np.asarray(gci)

    with np.errstate(divide='ignore', invalid='ignore'):
        Tent = np.log((gci[:, 0] * gci[:, 3]) / (gci[:, 1] * gci[:, 2]))
        # Calc correlation dimension
        D1 = h * (wavediff(gci[:, 1]) / wavediff(h)) / gci[:, 1]
        D3 = h * (wavediff(gci[:, 3]) / wavediff(h)) / gci[:, 3]
        D4 = h * (wavediff(gci[:, 4]) / wavediff(h)) / gci[:, 4]

    # Sync = np.log((D3 + D4) / (D1))
    Sync = D1 / (D3 + D4)

    return Tent, Sync


def Gci_eval(z0, z1, z2, z3, z4, h):
    g0 = np.mean(np.exp(z0 / h))
    g1 = np.mean(np.exp(z1 / h))
    g2 = np.mean(np.exp(z2 / h))
    g3 = np.mean(np.exp(z3 / h))
    g4 = np.mean(np.exp(z4 / h))
    return g0, g1, g2, g3, g4
