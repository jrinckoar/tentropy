#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='TentroPy',
      version='0.1',
      description='Transfer entropy',
      long_description=readme(),
      url='https://jrinckoar@bitbucket.org/jrinckoar/tentropy.git',
      author='Juan F. Restrepo',
      author_email='jrestrepo@bioingenieria.edu.ar',
      license='MIT',
      packages=['TentroPy'],
      keywords='transfer-entropy',
      install_requires=[
          'scipy',
          'numpy',
          'matplotlib',
          'tqdm',
          'PyWavelets',
          'mpmath',
      ],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      zip_safe=False)
