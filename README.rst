============
TentroPy
============
Juan F. Restrepo: jrestrepo@bioingenieria.edu.ar

Compilation of python routines to compute the tranfer entropy:

1. Shannon-Transfer-Entropy with Gaussian kernel.
2. Shannon-Transfer-Entropy with U kernel.
3. Renyi_q2-Transfer-Entropy with Gaussian kernel. 
4. Renyi_q2-Transfer-Entropy with U kernel. 

Bibliography
------------------
